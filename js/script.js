$(document).ready(() => {


////// SLICK SLIDESHOW
  $('.slideshow').slick({
    infinite:true,
    speed:900,
    autoplay:true
  });

////// HEADER MENU
  const menuDiv = $('#header-menu').offset().top;

  $(window).scroll(function() {
	if ($(document).scrollTop() > menuDiv) {
      $('.header-menu').addClass('pos-fixed');
      $('.header-fixed-logo').fadeIn(800);
      $('.slideshow, .section-fixed-social').addClass('mar-top');
    } else {
      $('.header-menu').removeClass('pos-fixed');
      $('.header-fixed-logo').hide();
      $('.slideshow, .section-fixed-social').removeClass('mar-top');
    }
  });

////// MENU BUTTON
  $('.menu-button_icon').click( function() {
    $(this).toggleClass('menu-open');
    $('.menu-button-wrapper').toggle();
  });

////// BACK TO TOP BUTTON
$(".footer__backtop").click(function() {
    $('html, body').animate({
        scrollTop: $("#section-head").offset().top
     }, 1000);
});

////// FANCYBOX
  $('[data-fancybox="images"]').fancybox({

    loop: true,
    buttons: [
      "zoom",
      //"share",
      "slideShow",
      "fullScreen",
      //"download",
      "thumbs",
      "close"
    ]
  });

});
